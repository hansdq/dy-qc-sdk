package address_provinceList_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AddressProvinceListResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AddressProvinceListData `json:"data"`
}
type DataItem struct {
	ProvinceId int64 `json:"province_id"`
	Province string `json:"province"`
	FatherId int64 `json:"father_id"`
}
type AddressProvinceListData struct {
	Data []DataItem `json:"data"`
}
