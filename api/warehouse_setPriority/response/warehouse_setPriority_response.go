package warehouse_setPriority_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseSetPriorityResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseSetPriorityData `json:"data"`
}
type WarehouseSetPriorityData struct {
}
