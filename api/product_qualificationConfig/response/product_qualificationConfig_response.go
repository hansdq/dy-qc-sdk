package product_qualificationConfig_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductQualificationConfigResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductQualificationConfigData `json:"data"`
}
type ConfigListItem struct {
	// 资质ID
	Key string `json:"key"`
	// 资质名
	Name string `json:"name"`
	// 填写提示
	TextList []string `json:"text_list"`
	// 是否必填
	IsRequired bool `json:"is_required"`
	// 支持的网页url类型，0:不支持;1:二手车质检报告url
	SupportWebUrlType string `json:"support_web_url_type"`
}
type ProductQualificationConfigData struct {
	// 资质列表
	ConfigList []ConfigListItem `json:"config_list"`
}
