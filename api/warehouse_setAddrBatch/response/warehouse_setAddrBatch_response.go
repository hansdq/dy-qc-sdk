package warehouse_setAddrBatch_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseSetAddrBatchResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseSetAddrBatchData `json:"data"`
}
type WarehouseSetAddrBatchData struct {
}
