package material_uploadImageSync_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type MaterialUploadImageSyncResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *MaterialUploadImageSyncData `json:"data"`
}
type MaterialUploadImageSyncData struct {
	// 素材id
	MaterialId string `json:"material_id"`
	// 素材所在文件夹id，0-素材中心的根目录；其他值-表示对应的文件夹id；
	FolderId string `json:"folder_id"`
	// 是否是新建，true-新建
	IsNew bool `json:"is_new"`
	// 素材审核状态; 1-等待审核; 2-审核中; 3-通过; 4-拒绝;
	AuditStatus int32 `json:"audit_status"`
	// 素材中心返回的url，正在下线，替代方案见：https://op.jinritemai.com/docs/notice-docs/5/2668
	ByteUrl string `json:"byte_url"`
}
