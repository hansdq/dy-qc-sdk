package address_update_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AddressUpdateResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AddressUpdateData `json:"data"`
}
type AddressUpdateData struct {
}
