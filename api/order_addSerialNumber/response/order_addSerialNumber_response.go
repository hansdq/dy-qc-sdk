package order_addSerialNumber_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderAddSerialNumberResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderAddSerialNumberData `json:"data"`
}
type OrderAddSerialNumberData struct {
}
