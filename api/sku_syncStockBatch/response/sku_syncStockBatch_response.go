package sku_syncStockBatch_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type SkuSyncStockBatchResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *SkuSyncStockBatchData `json:"data"`
}
type SkuSyncStockBatchData struct {
}
