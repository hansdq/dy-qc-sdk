package order_logisticsAddSinglePack_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderLogisticsAddSinglePackResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderLogisticsAddSinglePackData `json:"data"`
}
type OrderLogisticsAddSinglePackData struct {
	// 包裹id
	PackId string `json:"pack_id"`
}
