package product_editComponentTemplate_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductEditComponentTemplateResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductEditComponentTemplateData `json:"data"`
}
type ProductEditComponentTemplateData struct {
}
