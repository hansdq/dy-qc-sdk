package sku_editPrice_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type SkuEditPriceResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *SkuEditPriceData `json:"data"`
}
type SkuEditPriceData struct {
}
