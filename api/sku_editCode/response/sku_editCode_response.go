package sku_editCode_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type SkuEditCodeResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *SkuEditCodeData `json:"data"`
}
type SkuEditCodeData struct {
}
