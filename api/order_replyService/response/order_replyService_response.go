package order_replyService_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderReplyServiceResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderReplyServiceData `json:"data"`
}
type OrderReplyServiceData struct {
}
