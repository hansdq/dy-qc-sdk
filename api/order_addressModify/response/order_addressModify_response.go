package order_addressModify_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderAddressModifyResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderAddressModifyData `json:"data"`
}
type OrderAddressModifyData struct {
}
