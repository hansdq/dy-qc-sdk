package product_setOnline_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductSetOnlineResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductSetOnlineData `json:"data"`
}
type ProductSetOnlineData struct {
}
