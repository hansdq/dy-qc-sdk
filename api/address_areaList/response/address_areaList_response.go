package address_areaList_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AddressAreaListResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AddressAreaListData `json:"data"`
}
type DataItem struct {
	AreaId int64 `json:"area_id"`
	Area string `json:"area"`
	FatherId int64 `json:"father_id"`
}
type AddressAreaListData struct {
	Data []DataItem `json:"data"`
}
