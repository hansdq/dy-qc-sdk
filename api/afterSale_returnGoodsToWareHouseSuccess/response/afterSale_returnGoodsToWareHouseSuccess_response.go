package afterSale_returnGoodsToWareHouseSuccess_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleReturnGoodsToWareHouseSuccessResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleReturnGoodsToWareHouseSuccessData `json:"data"`
}
type AfterSaleReturnGoodsToWareHouseSuccessData struct {
}
