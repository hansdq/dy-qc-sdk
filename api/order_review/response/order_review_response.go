package order_review_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderReviewResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderReviewData `json:"data"`
}
type OrderReviewData struct {
}
