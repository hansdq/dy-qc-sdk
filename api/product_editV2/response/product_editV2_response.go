package product_editV2_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductEditV2Response struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductEditV2Data `json:"data"`
}
type ProductEditV2Data struct {
}
