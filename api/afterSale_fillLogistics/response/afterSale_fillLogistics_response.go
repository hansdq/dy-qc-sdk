package afterSale_fillLogistics_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleFillLogisticsResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleFillLogisticsData `json:"data"`
}
type AfterSaleFillLogisticsData struct {
}
