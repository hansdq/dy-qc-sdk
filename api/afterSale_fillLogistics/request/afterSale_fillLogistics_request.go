package afterSale_fillLogistics_request

import (
	"gitee.com/hansdq/dy-qc-sdk/api/afterSale_fillLogistics/response"
	"gitee.com/hansdq/dy-qc-sdk/core"
	"encoding/json"
)

type AfterSaleFillLogisticsRequest struct {
	doudian_sdk.BaseDoudianOpApiRequest
	Param *AfterSaleFillLogisticsParam 
}
func (c *AfterSaleFillLogisticsRequest) GetUrlPath() string{
	return "/afterSale/fillLogistics"
}


func New() *AfterSaleFillLogisticsRequest{
	request := &AfterSaleFillLogisticsRequest{
		Param: &AfterSaleFillLogisticsParam{},
	}
	request.SetConfig(doudian_sdk.GlobalConfig)
	request.SetClient(doudian_sdk.DefaultDoudianOpApiClient)
	return request

}


func (c *AfterSaleFillLogisticsRequest) Execute(accessToken *doudian_sdk.AccessToken) (*afterSale_fillLogistics_response.AfterSaleFillLogisticsResponse, error){
	responseJson, err := c.GetClient().Request(c, accessToken)
	if err != nil {
		return nil, err
	}
	response := &afterSale_fillLogistics_response.AfterSaleFillLogisticsResponse{}
	_ = json.Unmarshal([]byte(responseJson), response)
	return response, nil

}


func (c *AfterSaleFillLogisticsRequest) GetParamObject() interface{}{
	return c.Param
}


func (c *AfterSaleFillLogisticsRequest) GetParams() *AfterSaleFillLogisticsParam{
	return c.Param
}


type AfterSaleFillLogisticsParam struct {
	// 售后单ID
	AftersaleId int64 `json:"aftersale_id"`
	// 发货类型；适用场景： send_type=1：用于补寄商家发货 send_type=3：超市预约上门取货；退货退款和换货场景下商家帮买家填写退货物流信息； send_type=4：维修场景下商家帮买家填写退货物流信息；
	SendType int32 `json:"send_type"`
	// 物流公司编号
	CompanyCode string `json:"company_code"`
	// 物流单号
	TrackingNo string `json:"tracking_no"`
	// 预约上门取货时间戳，单位：秒（目前抖超小时达店铺使用）
	BookTimeBegin int64 `json:"book_time_begin"`
	// 预约上门取货时间戳，单位：秒（目前抖超小时达店铺使用）
	BookTimeEnd int64 `json:"book_time_end"`
	// 门店ID
	StoreId int64 `json:"store_id"`
}
