package order_logisticsAdd_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderLogisticsAddResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderLogisticsAddData `json:"data"`
}
type OrderLogisticsAddData struct {
}
