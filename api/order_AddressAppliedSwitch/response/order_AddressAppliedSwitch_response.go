package order_AddressAppliedSwitch_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderAddressAppliedSwitchResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderAddressAppliedSwitchData `json:"data"`
}
type OrderAddressAppliedSwitchData struct {
}
