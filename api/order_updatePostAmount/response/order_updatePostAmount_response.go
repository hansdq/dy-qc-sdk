package order_updatePostAmount_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderUpdatePostAmountResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderUpdatePostAmountData `json:"data"`
}
type OrderUpdatePostAmountData struct {
}
