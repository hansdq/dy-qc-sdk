package warehouse_removeAddr_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseRemoveAddrResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseRemoveAddrData `json:"data"`
}
type WarehouseRemoveAddrData struct {
}
