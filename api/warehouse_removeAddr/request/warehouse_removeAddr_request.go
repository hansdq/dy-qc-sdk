package warehouse_removeAddr_request

import (
	"gitee.com/hansdq/dy-qc-sdk/api/warehouse_removeAddr/response"
	"gitee.com/hansdq/dy-qc-sdk/core"
	"encoding/json"
)

type WarehouseRemoveAddrRequest struct {
	doudian_sdk.BaseDoudianOpApiRequest
	Param *WarehouseRemoveAddrParam 
}
func (c *WarehouseRemoveAddrRequest) GetUrlPath() string{
	return "/warehouse/removeAddr"
}


func New() *WarehouseRemoveAddrRequest{
	request := &WarehouseRemoveAddrRequest{
		Param: &WarehouseRemoveAddrParam{},
	}
	request.SetConfig(doudian_sdk.GlobalConfig)
	request.SetClient(doudian_sdk.DefaultDoudianOpApiClient)
	return request

}


func (c *WarehouseRemoveAddrRequest) Execute(accessToken *doudian_sdk.AccessToken) (*warehouse_removeAddr_response.WarehouseRemoveAddrResponse, error){
	responseJson, err := c.GetClient().Request(c, accessToken)
	if err != nil {
		return nil, err
	}
	response := &warehouse_removeAddr_response.WarehouseRemoveAddrResponse{}
	_ = json.Unmarshal([]byte(responseJson), response)
	return response, nil

}


func (c *WarehouseRemoveAddrRequest) GetParamObject() interface{}{
	return c.Param
}


func (c *WarehouseRemoveAddrRequest) GetParams() *WarehouseRemoveAddrParam{
	return c.Param
}


type Addr struct {
	// 一级地址id
	AddrId1 int64 `json:"addr_id1"`
	// 二级地址id
	AddrId2 int64 `json:"addr_id2"`
	// 三级地址id
	AddrId3 int64 `json:"addr_id3"`
	// 四级地址id
	AddrId4 int64 `json:"addr_id4"`
}
type WarehouseRemoveAddrParam struct {
	// 外部仓库ID
	OutWarehouseId string `json:"out_warehouse_id"`
	// 删除的地址结构体
	Addr *Addr `json:"addr"`
}
