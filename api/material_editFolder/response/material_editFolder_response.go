package material_editFolder_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type MaterialEditFolderResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *MaterialEditFolderData `json:"data"`
}
type MaterialEditFolderData struct {
}
