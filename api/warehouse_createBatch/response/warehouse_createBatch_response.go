package warehouse_createBatch_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseCreateBatchResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseCreateBatchData `json:"data"`
}
type WarehouseCreateBatchData struct {
	// key是outWarehouseId,value代表成功/失败
	Data map[string]bool `json:"data"`
}
