package product_setOffline_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductSetOfflineResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductSetOfflineData `json:"data"`
}
type ProductSetOfflineData struct {
}
