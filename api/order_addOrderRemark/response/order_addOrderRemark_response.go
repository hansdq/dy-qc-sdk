package order_addOrderRemark_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderAddOrderRemarkResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderAddOrderRemarkData `json:"data"`
}
type OrderAddOrderRemarkData struct {
}
