package afterSale_addOrderRemark_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleAddOrderRemarkResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleAddOrderRemarkData `json:"data"`
}
type AfterSaleAddOrderRemarkData struct {
}
