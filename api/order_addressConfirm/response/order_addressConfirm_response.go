package order_addressConfirm_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderAddressConfirmResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderAddressConfirmData `json:"data"`
}
type OrderAddressConfirmData struct {
}
