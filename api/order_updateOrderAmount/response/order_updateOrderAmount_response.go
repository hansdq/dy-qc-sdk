package order_updateOrderAmount_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderUpdateOrderAmountResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderUpdateOrderAmountData `json:"data"`
}
type OrderUpdateOrderAmountData struct {
}
