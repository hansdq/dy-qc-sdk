package order_updateOrderAmount_request

import (
	"gitee.com/hansdq/dy-qc-sdk/api/order_updateOrderAmount/response"
	"gitee.com/hansdq/dy-qc-sdk/core"
	"encoding/json"
)

type OrderUpdateOrderAmountRequest struct {
	doudian_sdk.BaseDoudianOpApiRequest
	Param *OrderUpdateOrderAmountParam 
}
func (c *OrderUpdateOrderAmountRequest) GetUrlPath() string{
	return "/order/updateOrderAmount"
}


func New() *OrderUpdateOrderAmountRequest{
	request := &OrderUpdateOrderAmountRequest{
		Param: &OrderUpdateOrderAmountParam{},
	}
	request.SetConfig(doudian_sdk.GlobalConfig)
	request.SetClient(doudian_sdk.DefaultDoudianOpApiClient)
	return request

}


func (c *OrderUpdateOrderAmountRequest) Execute(accessToken *doudian_sdk.AccessToken) (*order_updateOrderAmount_response.OrderUpdateOrderAmountResponse, error){
	responseJson, err := c.GetClient().Request(c, accessToken)
	if err != nil {
		return nil, err
	}
	response := &order_updateOrderAmount_response.OrderUpdateOrderAmountResponse{}
	_ = json.Unmarshal([]byte(responseJson), response)
	return response, nil

}


func (c *OrderUpdateOrderAmountRequest) GetParamObject() interface{}{
	return c.Param
}


func (c *OrderUpdateOrderAmountRequest) GetParams() *OrderUpdateOrderAmountParam{
	return c.Param
}


type UpdateDetailItem struct {
	OrderId string `json:"order_id"`
	DiscountAmount int64 `json:"discount_amount"`
}
type OrderUpdateOrderAmountParam struct {
	Pid string `json:"pid"`
	UpdateDetail []UpdateDetailItem `json:"update_detail"`
}
