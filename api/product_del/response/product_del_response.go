package product_del_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductDelResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductDelData `json:"data"`
}
type ProductDelData struct {
}
