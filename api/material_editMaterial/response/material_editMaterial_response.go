package material_editMaterial_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type MaterialEditMaterialResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *MaterialEditMaterialData `json:"data"`
}
type MaterialEditMaterialData struct {
}
