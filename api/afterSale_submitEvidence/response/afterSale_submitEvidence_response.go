package afterSale_submitEvidence_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleSubmitEvidenceResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleSubmitEvidenceData `json:"data"`
}
type AfterSaleSubmitEvidenceData struct {
}
