package material_easyShuttle_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type MaterialEasyShuttleResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *MaterialEasyShuttleData `json:"data"`
}
type MaterialEasyShuttleData struct {
}
