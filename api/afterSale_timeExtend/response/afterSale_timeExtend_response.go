package afterSale_timeExtend_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleTimeExtendResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleTimeExtendData `json:"data"`
}
type AfterSaleTimeExtendData struct {
}
