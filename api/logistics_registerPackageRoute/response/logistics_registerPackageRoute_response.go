package logistics_registerPackageRoute_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type LogisticsRegisterPackageRouteResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *LogisticsRegisterPackageRouteData `json:"data"`
}
type LogisticsRegisterPackageRouteData struct {
}
