package spu_createSpu_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type SpuCreateSpuResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *SpuCreateSpuData `json:"data"`
}
type SpuCreateSpuData struct {
	// SPU的ID
	SpuId string `json:"spu_id"`
}
