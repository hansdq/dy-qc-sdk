package order_logisticsEdit_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderLogisticsEditResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderLogisticsEditData `json:"data"`
}
type OrderLogisticsEditData struct {
}
