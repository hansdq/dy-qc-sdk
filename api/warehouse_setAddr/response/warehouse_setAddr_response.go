package warehouse_setAddr_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseSetAddrResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseSetAddrData `json:"data"`
}
type WarehouseSetAddrData struct {
}
