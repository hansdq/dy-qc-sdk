package product_editBuyerLimit_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductEditBuyerLimitResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductEditBuyerLimitData `json:"data"`
}
type ProductEditBuyerLimitData struct {
}
