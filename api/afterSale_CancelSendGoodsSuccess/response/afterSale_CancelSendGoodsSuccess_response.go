package afterSale_CancelSendGoodsSuccess_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleCancelSendGoodsSuccessResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleCancelSendGoodsSuccessData `json:"data"`
}
type AfterSaleCancelSendGoodsSuccessData struct {
}
