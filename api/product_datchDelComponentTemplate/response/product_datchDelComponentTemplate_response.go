package product_datchDelComponentTemplate_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type ProductDatchDelComponentTemplateResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *ProductDatchDelComponentTemplateData `json:"data"`
}
type ProductDatchDelComponentTemplateData struct {
}
