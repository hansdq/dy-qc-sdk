package promise_deliveryList_request

import (
	"gitee.com/hansdq/dy-qc-sdk/api/promise_deliveryList/response"
	"gitee.com/hansdq/dy-qc-sdk/core"
	"encoding/json"
)

type PromiseDeliveryListRequest struct {
	doudian_sdk.BaseDoudianOpApiRequest
	Param *PromiseDeliveryListParam 
}
func (c *PromiseDeliveryListRequest) GetUrlPath() string{
	return "/promise/deliveryList"
}


func New() *PromiseDeliveryListRequest{
	request := &PromiseDeliveryListRequest{
		Param: &PromiseDeliveryListParam{},
	}
	request.SetConfig(doudian_sdk.GlobalConfig)
	request.SetClient(doudian_sdk.DefaultDoudianOpApiClient)
	return request

}


func (c *PromiseDeliveryListRequest) Execute(accessToken *doudian_sdk.AccessToken) (*promise_deliveryList_response.PromiseDeliveryListResponse, error){
	responseJson, err := c.GetClient().Request(c, accessToken)
	if err != nil {
		return nil, err
	}
	response := &promise_deliveryList_response.PromiseDeliveryListResponse{}
	_ = json.Unmarshal([]byte(responseJson), response)
	return response, nil

}


func (c *PromiseDeliveryListRequest) GetParamObject() interface{}{
	return c.Param
}


func (c *PromiseDeliveryListRequest) GetParams() *PromiseDeliveryListParam{
	return c.Param
}


type PromiseDeliveryListParam struct {
	// 商品ID
	ProductId int64 `json:"product_id"`
	// 页码，从1开始
	Page int64 `json:"page"`
	// 每页数量
	Size int64 `json:"size"`
}
