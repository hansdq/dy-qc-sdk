package warehouse_createV2_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseCreateV2Response struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseCreateV2Data `json:"data"`
}
type WarehouseCreateV2Data struct {
	// 内部仓id
	WarehouseId string `json:"warehouse_id"`
}
