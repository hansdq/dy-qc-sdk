package warehouse_create_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseCreateResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseCreateData `json:"data"`
}
type WarehouseCreateData struct {
	// 仓库id
	Data int64 `json:"data"`
}
