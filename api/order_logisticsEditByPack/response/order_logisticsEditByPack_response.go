package order_logisticsEditByPack_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type OrderLogisticsEditByPackResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *OrderLogisticsEditByPackData `json:"data"`
}
type OrderLogisticsEditByPackData struct {
}
