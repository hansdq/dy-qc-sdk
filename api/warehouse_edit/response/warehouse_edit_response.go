package warehouse_edit_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type WarehouseEditResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *WarehouseEditData `json:"data"`
}
type WarehouseEditData struct {
	// 修改结果
	Data bool `json:"data"`
}
