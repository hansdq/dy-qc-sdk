package freightTemplate_update_request

import (
	"gitee.com/hansdq/dy-qc-sdk/api/freightTemplate_update/response"
	"gitee.com/hansdq/dy-qc-sdk/core"
	"encoding/json"
)

type FreightTemplateUpdateRequest struct {
	doudian_sdk.BaseDoudianOpApiRequest
	Param *FreightTemplateUpdateParam 
}
func (c *FreightTemplateUpdateRequest) GetUrlPath() string{
	return "/freightTemplate/update"
}


func New() *FreightTemplateUpdateRequest{
	request := &FreightTemplateUpdateRequest{
		Param: &FreightTemplateUpdateParam{},
	}
	request.SetConfig(doudian_sdk.GlobalConfig)
	request.SetClient(doudian_sdk.DefaultDoudianOpApiClient)
	return request

}


func (c *FreightTemplateUpdateRequest) Execute(accessToken *doudian_sdk.AccessToken) (*freightTemplate_update_response.FreightTemplateUpdateResponse, error){
	responseJson, err := c.GetClient().Request(c, accessToken)
	if err != nil {
		return nil, err
	}
	response := &freightTemplate_update_response.FreightTemplateUpdateResponse{}
	_ = json.Unmarshal([]byte(responseJson), response)
	return response, nil

}


func (c *FreightTemplateUpdateRequest) GetParamObject() interface{}{
	return c.Param
}


func (c *FreightTemplateUpdateRequest) GetParams() *FreightTemplateUpdateParam{
	return c.Param
}


type FreightTemplateUpdateParam struct {
	// 运费模板相关
	Template *Template `json:"template"`
	// 运费模板规则信息；每种类型模板可创建的规则类型: 阶梯计价模板-默认规则，普通计价规则，包邮规则，限运规则;固定运费模板-包邮规则，限运规则;固定运费模板-包邮规则，限运规则;包邮模板-限运规则;货到付款模板-限运规则
	Columns []ColumnsItem `json:"columns"`
}
type Template struct {
	// 要更新的运费模板id
	Id int64 `json:"id"`
	// 模板名称
	TemplateName string `json:"template_name"`
	// 发货省份
	ProductProvince int64 `json:"product_province"`
	// 发货城市
	ProductCity int64 `json:"product_city"`
	// 计价方式-1.按重量 2.按数量
	CalculateType int64 `json:"calculate_type"`
	// 快递方式-1.快递
	TransferType int64 `json:"transfer_type"`
	// 模板类型-0:阶梯计价 1:固定运费 2:卖家包邮 3:货到付款
	RuleType int64 `json:"rule_type"`
	// 固定运费金额(单位:分) 固定运费模板必填 1-9900之间的整数
	FixedAmount int64 `json:"fixed_amount"`
}
type ColumnsItem struct {
	// 首重(单位:kg) 按重量计价必填 0.1-999.9之间的小数，小数点后一位
	FirstWeight float64 `json:"first_weight"`
	// 首重价格(单位:元) 按重量计价必填 0.00-30.00之间的小数，小数点后两位
	FirstWeightPrice float64 `json:"first_weight_price"`
	// 首件数量(单位:个) 按数量计价必填 1-999的整数
	FirstNum int64 `json:"first_num"`
	// 首件价格(单位:元)按数量计价必填 0.00-30.00之间的小数，小数点后两位
	FirstNumPrice float64 `json:"first_num_price"`
	// 续重(单位:kg) 按重量计价必填 0.1-999.9之间的小数，小数点后一位
	AddWeight float64 `json:"add_weight"`
	// 续重价格(单位:元) 按重量计价必填 0.00-30.00之间的小数，小数点后两位
	AddWeightPrice float64 `json:"add_weight_price"`
	// 续件(单位：个)calculate_type=2必填 1-999的整数
	AddNum int64 `json:"add_num"`
	// 续件价格(单位:元) 按数量计价必填 0.00-30.00之间的小数，小数点后两位
	AddNumPrice float64 `json:"add_num_price"`
	// 是否默认计价方式(1:是；0:不是)
	IsDefault int64 `json:"is_default"`
	// 是否限运规则
	IsLimited bool `json:"is_limited"`
	// 当前规则生效的地址，非默认规则必填。map<i64, map<i64, map<i64, list<i64>>>>的json格式，省->市->区->街道，仅限售规则支持四级街道
	RuleAddress string `json:"rule_address"`
	// 是否包邮规则
	IsOverFree bool `json:"is_over_free"`
	// 满xx重量包邮(单位:kg)0.1-10.0之间的小数，小数点后一位
	OverWeight float64 `json:"over_weight"`
	// 满xx金额包邮(单位:分)10-99900的整数
	OverAmount int64 `json:"over_amount"`
	// 满xx件包邮 1-10之间的整数
	OverNum int64 `json:"over_num"`
}
