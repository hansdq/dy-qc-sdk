package address_create_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AddressCreateResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AddressCreateData `json:"data"`
}
type AddressCreateData struct {
	// 新建地址ID
	AddressId int64 `json:"address_id"`
}
