package sku_syncStock_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type SkuSyncStockResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *SkuSyncStockData `json:"data"`
}
type SkuSyncStockData struct {
}
