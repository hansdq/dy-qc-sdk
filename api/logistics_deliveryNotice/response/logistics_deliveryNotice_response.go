package logistics_deliveryNotice_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type LogisticsDeliveryNoticeResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *LogisticsDeliveryNoticeData `json:"data"`
}
type LogisticsDeliveryNoticeData struct {
	// 是否成功
	Result bool `json:"result"`
}
