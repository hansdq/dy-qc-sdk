package afterSale_openOutAfterSale_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AfterSaleOpenOutAfterSaleResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AfterSaleOpenOutAfterSaleData `json:"data"`
}
type AfterSaleOpenOutAfterSaleData struct {
}
