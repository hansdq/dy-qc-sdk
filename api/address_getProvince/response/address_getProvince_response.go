package address_getProvince_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type AddressGetProvinceResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *AddressGetProvinceData `json:"data"`
}
type DataItem struct {
	// 省份id
	ProvinceId int64 `json:"province_id"`
	// 省份
	Province string `json:"province"`
}
type AddressGetProvinceData struct {
	// data
	Data []DataItem `json:"data"`
}
