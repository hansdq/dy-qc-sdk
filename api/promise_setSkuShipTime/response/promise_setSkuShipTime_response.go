package promise_setSkuShipTime_response

import (
	"gitee.com/hansdq/dy-qc-sdk/core"
)

type PromiseSetSkuShipTimeResponse struct {
	doudian_sdk.BaseDoudianOpApiResponse
	Data *PromiseSetSkuShipTimeData `json:"data"`
}
type PromiseSetSkuShipTimeData struct {
}
